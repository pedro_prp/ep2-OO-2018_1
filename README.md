## EP1_2018/1
<br/>
### Utilização
<br/>
Para utilização do jogo é recomendável o uso da IDE Eclipse.<br/>

	Ao iniciar o Jogo, o usuario deve primeiro clicar no botão "Abrir mapa"
	E aguardar a abertura de uma janela do JFile Chooser, logo após isso, o botão "Jogar"
	será liberado.
<br/>
### Identificação
<br/>
Exercício Prático apresentado na matéria de OO pelo professor Renato Sampaio e produzido inteiramente em **Java**.<br/>
<br/>**Aluno:** Pedro Rodrigues Pereira - **matrícula:** 17/0062686.<br/>